#!/bin/sh

. ./public/_export.source
echo 'test'
echo $PATH
ls ./public/

cat > ./public/index.html <<EOF
<!DOCTYPE html>
<html>
<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-52437368-5"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-52437368-5');
</script>
<meta charset="UTF-8">
<title>Test-html</title>
</head>
<body>
<ul>
EOF



(
    cd ./public;
    for x in $( find files -name '*.mki3d' ); do
	echo '<li> <a href="'$x'"> ' $x '</a></li>' ;
    done;
) >> ./public/index.html

cat >> ./public/index.html <<EOF
</ul>
</body>
</html>

EOF

less ./public/index.html


